# Time til quota

A silly program to display how much time needs yet to be done before a quota is met.

## Use

General command structure:

    ttq QUOTA
    ttq QUOTA [ {CLOCKIN CLOCKOUT} ... ] [CLOCKIN]

Start it with a quota in `hh:mm`, and optionally times worked

    ttq 7:30

or immediately specify times worked, as a sequence of clock-in/clock-out times

    ttq 07:30 08:30 09:10 9:22 12:35

You will then be prompted for input. Either add times you have worked, in pairs of stop / start, space- or hyphen-separated

    > 09:17 12:42
    > 13:15-15:40

or state the time you last started, to display time at which quota will be met

    > 13:24

You can remove time by stating later time first, followed by earlier time. Use a base of 00:00 to make it easier. Remove 1h from worked time:

    > 01:00 00:00

Type `quit` or `exit` to end the program, or end input.
