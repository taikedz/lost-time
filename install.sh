#!/usr/bin/env bash

cd "$(dirname "$0")"

command="$HOME/.local/bin/ttq"

[[ "$UID" != 0 ]] || {
    command="/usr/local/bin/ttq"
}

mkdir -p "$(dirname "$command")"
cp "src/time-til-quota" "$command"

echo "-- Installed to $command --"
